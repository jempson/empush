#!/usr/bin/env python
# encoding: utf-8

"""
@author: wuyang
@software: PyCharm
@file: settings.py
@time: 2017/10/25 8:18
@文件描述：所有配置项目在这里
"""
import os
source_path = "/home/wuyangdev/test/"     # 被监控目录  特别注意此处反斜杠
des_ip = "192.168.150.23"                  # 同步主机地址
des_path = "/home/github/test/"           # 同步目录 特别注意此处反斜杠
des_port = "22"                             # 同步主机端口

daemonize=False                         # 设置是否后台运行True为后台运行，调试的时候设置为False
pid_file='/tmp/pyinotify.pid'        # 后台运行时pid存放，防止启动多个进程

log_dir = "/var/log/"                 # 日志存放路径，必须存在
# 日志配置文件
logging_conf = {
    'version': 1,  # 版本始终是1
    'disable_existing_loggers': False,
    # 配置格式
    'formatters': {
        'common': { # 自定义常用格式
            'class': 'logging.Formatter',
            'format': '%(asctime)s %(pathname)s:%(lineno)d[%(levelname)s] - %(message)s',
        },
        'standard': {  # 标准格式
            'format': '%(asctime)s [%(threadName)s:%(thread)d] [%(name)s:%(lineno)d] [%(module)s:%(funcName)s] [%(levelname)s]- %(message)s'
        },

    },
    # 配置控制项
    'handlers': {
        'commonhandler': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': log_dir + 'common.log',
            'maxBytes': 1024*1024*5,   # 不可以写成字符类型
            'backupCount': 3,  # 不可以写成字符类型
            'formatter': 'common',
        },
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'standard',
        },
    },
    # 配置管理入口
    'loggers': {
        'common_logger': {
            'level': 'INFO',
            'handlers': ['commonhandler','console']
        },
    }


}